import sys
import csv
import yaml

# import ldscriptures as lds

filename = "abbr_mapping.csv"
lds_path = sys.argv[1]

print("**=====================================================**")
print("\n\n")
print("getting data for: " + lds_path + " ...\n\n")

print("link: https://www.churchofjesuschrist.org/study/scriptures" + lds_path + "\n\n")

parsed_path = lds_path.split("/")

def get_book(book_abbr):
	with open(filename,"r") as infile:
		reader = csv.reader(infile)
		for row in reader:
			if row[0] == book_abbr:
				return row[1]

def get_sort(book_abbr):
	with open(filename,"r") as infile:
		reader = csv.reader(infile)
		for row in reader:
			if row[0] == book_abbr:
				return int(row[2])

def parse_verse_str(verse_str):
	verses_dict = dict()
	verses_dict['chapter'] = int(verse_str.split(".")[0])
	verses_dict['verses'] = verse_str.split(".")[1].replace("p","").split("#")[0]
	verses_dict['first_verse'] = int(verses_dict['verses'].partition("-")[0])
	return verses_dict

verses_dict = parse_verse_str(parsed_path[3])

lds_path_dict = {
	'sw_abbr': parsed_path[1],
	'book_abbr': parsed_path[2],
	'book_sort': get_sort(parsed_path[2]),
	'book_name': get_book(parsed_path[2]),
	'chapter': verses_dict['chapter'],
	'verses': verses_dict['verses'],
	'first_verse': verses_dict['first_verse'],
}

ref_template = '{book_name} {chapter}:{verses}'
reference = ref_template.format(**lds_path_dict)
# text = lds.get(reference)
sort_value = lds_path_dict['book_sort'] + lds_path_dict['chapter'] + (lds_path_dict['first_verse']/1000)
entry_dict = { sort_value: {
	  'lds_path': lds_path,
	  'reference': reference,
	  'text': "",
	  'mastery': False,
	  'topics': []
  }
}

print(yaml.dump(entry_dict))
print("=========================\n\n")
# print(text.text)