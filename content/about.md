+++
title = "About Liberation Scripture Mastery"
date = "2020-09-06"
+++

## Scripture Mastery

There are many ways the scriptures have been read and many of us were taught to read them a certain way in seminary. The Scripture Mastery program in LDS seminary is a program for memorizing scriptures and also serves as a way to focus on particular topics in the scriptures and by doing so elevates the importance of certain scriptures.

This website means to begin a project to index and share the scriptures with a message of liberation, which message is central to the Gospel of Jesus Christ and it's restoration through the prophet Joseph Smith. This message is frequently overlooked in our communities even though, as latter-day saints, our mission is to build the kingdom of God. A kingdom for the liberation of all of God's Children.

## A Gospel of Liberation

What is a message of liberation? The best way to see is to look at the scriptures themselves. Jesus described his own mission in terms of liberation when he read from Isaiah 61 in the synagogue at Nazareth. 

Here is the scripture describing this reading in Luke 4

> And he came to Nazareth, where he had been brought up: and, as his custom was, he went into the synagogue on the sabbath day, and stood up for to read. And there was delivered unto him the book of the prophet Esaias. And when he had opened the book, he found the place where it was written, The Spirit of the Lord is upon me, because he hath anointed me to preach the gospel to the poor; he hath sent me to heal the brokenhearted, to preach deliverance to the captives, and recovering of sight to the blind, to set at liberty them that are bruised, To preach the acceptable year of the Lord. And he closed the book, and he gave it again to the minister, and sat down. And the eyes of all them that were in the synagogue were fastened on him. And he began to say unto them, This day is this scripture fulfilled in your ears.

Jesus asks us to join him in this work and, in Mathew 25 in the parable of the sheep and the goats, says he will judge us by how well we help the poor, incarcerated, or otherwise marginalized among us. 

> When the Son of man shall come in his glory, and all the holy angels with him, then shall he sit upon the throne of his glory: And before him shall be gathered all nations: and he shall separate them one from another, as a shepherd divideth his sheep from the goats: And he shall set the sheep on his right hand, but the goats on the left. Then shall the King say unto them on his right hand, Come, ye blessed of my Father, inherit the kingdom prepared for you from the foundation of the world: For I was an hungred, and ye gave me meat: I was thirsty, and ye gave me drink: I was a stranger, and ye took me in: Naked, and ye clothed me: I was sick, and ye visited me: I was in prison, and ye came unto me. Then shall the righteous answer him, saying, Lord, when saw we thee an hungred, and fed thee? or thirsty, and gave thee drink? When saw we thee a stranger, and took thee in? or naked, and clothed thee? Or when saw we thee sick, or in prison, and came unto thee? And the King shall answer and say unto them, Verily I say unto you, Inasmuch as ye have done it unto one of the least of these my brethren, ye have done it unto me.

The scriptures this project means to highlight are those scriptures which align to this message of liberation and the call to care for the least among us. If we can commit these scriptures to memory then hopefully they can take greater hold on our hearts and make us better disciples of Jesus so that we can work with him in building Zion, with one heart and one mind with no poor among us. 

## Contributing

This project is being accomplished in the following phases:

Phase 1 - Indexing scriptures of liberation.

Phase 2 - Selecting scriptures for mastery

Phase 3 - Make good use in our communities both the index and the mastery list.

**Current Phase: Phase 1**

If you'd like to contribute to phase 1 please submit scriptures of liberation. You can check the [current submission list](/scriptures) to see if we've already indexed it, if not you can submit scriptures by sending an email to liberationscript@gmail.com.

If you're using the gospel library app you can easily submit scriptures to us from there by highlighting the verses and sharing via email.

![sub1](/img/sub1.png)  

![sub2](/img/sub2.png)
